#include <Snake.h>
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    Snake wind;
    wind.resize(500,500);
    wind.show();

    return a.exec();
}
