#include "Snake.h"


Snake::Snake(QWidget *parent):QWidget(parent)
{
    startGame();
}
void Snake::startGame()
{
    dots = 3;
    setApple();
    pressedKey = Qt::Key_Left;
    for( int startDots = 0; startDots < dots; startDots++)
    {
        x[startDots] = 250 + startDots*xRectSize ;
        y[startDots] = 250;
    }
    startTimer(gameSpeed);
}
void Snake::timerEvent(QTimerEvent *e)
{
    Q_UNUSED(e);

    move();
    checkApple();
    repaint();
}
void Snake::paintEvent(QPaintEvent *e)
{
    Q_UNUSED(e);

    painter();
}
void Snake::keyPressEvent(QKeyEvent *e)
{
    pressedKey = e->key();
}
void Snake::painter()
{
    QPainter painter(this);

    for (int copyDots = 0; copyDots < dots; copyDots++) {
         painter.fillRect(x[copyDots],y[copyDots],xRectSize,
                          yRectSize, Qt::yellow);
    }

    painter.fillRect(xApple,yApple,xRectSize,yRectSize,Qt::red);
}

void Snake::move()
{
    for (int i = dots; i > 0; i--) {
        x[i] = x[i-1];
        y[i] = y[i-1];
    }
    if (pressedKey != 0)
    {
        if(pressedKey == Qt::Key_Left)
             x[0]-=xRectSize;
        if(pressedKey==Qt::Key_Right)
             x[0]+=xRectSize;
        if(pressedKey == Qt::Key_Up)
            y[0]-=xRectSize;
         if(pressedKey == Qt::Key_Down)
              y[0]+=xRectSize;
    }
}
void Snake::setApple()
{
    xApple = (qrand()%xRandValue)*xRectSize;
    yApple = (qrand()%yRandValue)*xRectSize;

}
void Snake::checkApple()
{
    if((x[0]==xApple)&&(y[0]==yApple))
    {
        dots++;
        setApple();
    }
}
