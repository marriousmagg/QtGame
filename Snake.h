#pragma once
#include <QWidget>
#include <QPainter>
#include <QKeyEvent>
#include <QTime>
class Snake : public QWidget
{
public:
    Snake(QWidget *parent = nullptr);

protected:
    void timerEvent(QTimerEvent *);
    void paintEvent(QPaintEvent *);
    void keyPressEvent(QKeyEvent *);

private:
    static const int DOTS = 1000;
    static const int xRectSize = 10;
    static const int yRectSize = 10;

    int x[DOTS];
    int y[DOTS];

    int dots;
    int pressedKey;
    int xApple;
    int yApple;

    int xCoord = 50;
    int yCoord = 50;
    int gameSpeed = 250;
    int xRandValue = 29;
    int yRandValue = 29;

    void painter();
    void startGame();
    void move();
    void setApple();
    void checkApple();

};

